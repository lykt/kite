# Keymap iterator

## Description
Basic keyboard layout converter and USB HID keycode remapper.

More information at [https://lykt.xyz/kite/](https://lykt.xyz/kite/)

## Project status
* Support for [nanoCH32V203](https://github.com/wuxx/nanoCH32V203) RISC-V MCU board landed on [ch32v203](https://gitlab.com/lykt/kite/-/tree/ch32v203) branch.
* Support for [nanoCH32V305](https://github.com/wuxx/nanoCH32V305) RISC-V MCU board landed on [ch32v305](https://gitlab.com/lykt/kite/-/tree/ch32v305) branch.
