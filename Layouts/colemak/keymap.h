﻿#include "stdint.h"
#include "usb_hid_keys.h"

uint8_t  KITE_KEYMAP[ 38 ] = {
	KEY_D, KEY_S,
	KEY_E, KEY_F,
	KEY_F, KEY_T,
	KEY_G, KEY_D,
	KEY_I, KEY_U,
	KEY_J, KEY_N,
	KEY_K, KEY_E,
	KEY_L, KEY_I,
	KEY_N, KEY_K,
	KEY_O, KEY_Y,
	KEY_P, KEY_SEMICOLON,
	KEY_R, KEY_P,
	KEY_S, KEY_R,
	KEY_T, KEY_G,
	KEY_U, KEY_L,
	KEY_Y, KEY_J,
	KEY_SEMICOLON, KEY_O,
	KEY_CAPSLOCK, KEY_BACKSPACE,
	KEY_PAUSE, KEY_CAPSLOCK
};

/*
 * Colemak layout
 */
