﻿#include "stdint.h"
#include "usb_hid_keys.h"

uint8_t  KITE_KEYMAP[ 58 ] = {
	KEY_A, KEY_K,
	KEY_B, KEY_W,
	KEY_C, KEY_P,
	KEY_D, KEY_T,
	KEY_E, KEY_G,
	KEY_G, KEY_S,
	KEY_H, KEY_I,
	KEY_I, KEY_O,
	KEY_K, KEY_E,
	KEY_L, KEY_R,
	KEY_M, KEY_LEFTBRACE,
	KEY_N, KEY_Y,
	KEY_O, KEY_H,
	KEY_P, KEY_X,
	KEY_Q, KEY_C,
	KEY_R, KEY_M,
	KEY_S, KEY_N,
	KEY_T, KEY_D,
	KEY_U, KEY_A,
	KEY_V, KEY_B,
	KEY_W, KEY_V,
	KEY_X, KEY_COMMA,
	KEY_Y, KEY_U,
	KEY_Z, KEY_DOT,
	KEY_COMMA, KEY_SEMICOLON,
	KEY_DOT, KEY_Z,
	KEY_LEFTBRACE, KEY_SLASH,
	KEY_SEMICOLON, KEY_L,
	KEY_SLASH, KEY_Q
};

/*
 * Skarp layout (nb)
 */
