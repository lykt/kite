﻿#include "stdint.h"
#include "usb_hid_keys.h"

uint8_t  KITE_KEYMAP[ 60 ] = {
	KEY_A, KEY_O,
	KEY_B, KEY_V,
	KEY_C, KEY_Y,
	KEY_D, KEY_E,
	KEY_E, KEY_U,
	KEY_F, KEY_F,
	KEY_G, KEY_N,
	KEY_H, KEY_R,
	KEY_I, KEY_L,
	KEY_J, KEY_S,
	KEY_K, KEY_T,
	KEY_L, KEY_I,
	KEY_M, KEY_G,
	KEY_N, KEY_W,
	KEY_O, KEY_K,
	KEY_P, KEY_SEMICOLON,
	KEY_R, KEY_H,
	KEY_S, KEY_A,
	KEY_T, KEY_P,
	KEY_U, KEY_C,
	KEY_V, KEY_B,
	KEY_W, KEY_X,
	KEY_X, KEY_J,
	KEY_Y, KEY_M,
	KEY_HANGEUL, KEY_BACKSPACE,
	KEY_HANJA, KEY_DELETE,
	KEY_SEMICOLON, KEY_D
};

/*
 * FNRS layout
 */
