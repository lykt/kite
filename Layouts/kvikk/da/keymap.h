﻿#include "stdint.h"
#include "usb_hid_keys.h"

uint8_t  KITE_KEYMAP[ 58 ] = {
	KEY_A, KEY_L,
	KEY_B, KEY_Y,
	KEY_C, KEY_APOSTROPHE,
	KEY_D, KEY_E,
	KEY_E, KEY_U,
	KEY_F, KEY_A,
	KEY_G, KEY_I,
	KEY_H, KEY_D,
	KEY_I, KEY_M,
	KEY_J, KEY_T,
	KEY_K, KEY_N,
	KEY_L, KEY_S,
	KEY_M, KEY_P,
	KEY_N, KEY_B,
	KEY_O, KEY_H,
	KEY_P, KEY_C,
	KEY_Q, KEY_X,
	KEY_R, KEY_O,
	KEY_S, KEY_R,
	KEY_T, KEY_SEMICOLON,
	KEY_U, KEY_K,
	KEY_V, KEY_LEFTBRACE,
	KEY_W, KEY_J,
	KEY_X, KEY_Z,
	KEY_Y, KEY_F,
	KEY_Z, KEY_Q,
	KEY_APOSTROPHE, KEY_V,
	KEY_LEFTBRACE, KEY_W,
	KEY_SEMICOLON, KEY_G
};

/*
 * Kvikk layout (da)
 */
