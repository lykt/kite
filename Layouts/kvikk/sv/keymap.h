﻿#include "stdint.h"
#include "usb_hid_keys.h"

uint8_t  KITE_KEYMAP[ 58 ] = {
	KEY_A, KEY_L,
	KEY_B, KEY_Y,
	KEY_C, KEY_LEFTBRACE,
	KEY_D, KEY_A,
	KEY_E, KEY_O,
	KEY_F, KEY_E,
	KEY_G, KEY_I,
	KEY_H, KEY_D,
	KEY_I, KEY_M,
	KEY_J, KEY_T,
	KEY_K, KEY_N,
	KEY_L, KEY_S,
	KEY_M, KEY_P,
	KEY_N, KEY_F,
	KEY_O, KEY_B,
	KEY_P, KEY_J,
	KEY_Q, KEY_X,
	KEY_R, KEY_APOSTROPHE,
	KEY_S, KEY_R,
	KEY_T, KEY_U,
	KEY_U, KEY_K,
	KEY_V, KEY_SEMICOLON,
	KEY_W, KEY_C,
	KEY_X, KEY_Z,
	KEY_Y, KEY_H,
	KEY_Z, KEY_Q,
	KEY_APOSTROPHE, KEY_V,
	KEY_LEFTBRACE, KEY_W,
	KEY_SEMICOLON, KEY_G
};

/*
 * Kvikk layout (sv)
 */
